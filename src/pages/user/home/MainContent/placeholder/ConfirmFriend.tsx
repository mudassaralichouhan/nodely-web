import React from "react";
import { Link } from "react-router-dom";

const confirmFriendList = [
  {
    name: 'Anthony Daugloi',
    mutualFriends: 12,
    photo: '/assets/images/user-7.png',
  },
  {
    name: 'David Agfree',
    mutualFriends: 12,
    photo: '/assets/images/user-8.png',
  },
  {
    name: 'Hugury Daugloi',
    mutualFriends: 12,
    photo: '/assets/images/user-12.png',
  },
];

const ConfirmFriend: React.FC = () => {
  return (
    <div className="card w-100 shadow-xss rounded-xxl border-0 p-0 ">
      <div className="card-body d-flex align-items-center p-4 mb-0">
        <h4 className="fw-700 mb-0 font-xssss text-grey-900">
          Confirm Friend
        </h4>
        <Link
          to={"/friends"}
          className="fw-600 ms-auto font-xssss text-primary"
        >
          See all
        </Link>
      </div>
      {confirmFriendList.map((friend, index) => (
        <div className="card-body bg-transparent-card d-flex p-3 bg-greylight ms-3 me-3 rounded-3 mb-3" key={index}>
          <figure className="avatar me-2 mb-0">
            <img
              src={friend.photo}
              alt="image"
              className="shadow-sm rounded-circle w45"
            />
          </figure>
          <h4 className="fw-700 text-grey-900 font-xssss mt-2">
            {friend.name}
            <span className="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">
              {friend.mutualFriends} mutual friend{friend.mutualFriends !== 1 ? 's' : ''}
            </span>
          </h4>
          <a
            href="#"
            className="btn-round-sm bg-white text-grey-900 feather-chevron-right font-xss ms-auto mt-2"
          />
        </div>
      ))}
    </div>
  );
};

export default ConfirmFriend;
