import React from "react";

import Contacts from "@/pages/user/home/RightChatWrapper/Contacts.tsx";
import Pages from "@/pages/user/home/RightChatWrapper/Pages.tsx";
import Group from "@/pages/user/home/RightChatWrapper/Groups.tsx";

const RightChatWrapper: React.FC = () => {
  return (
    <React.Fragment>
      <div className="right-chat nav-wrap mt-2 right-scroll-bar active-sidebar">
        <div className="middle-sidebar-right-content bg-white shadow-xss rounded-xxl">
          
          <div className="preloader-wrap p-3" style={{ display: "none" }}>
            <div className="box shimmer">
              <div className="lines">
                <div className="line s_shimmer"></div>
                <div className="line s_shimmer"></div>
                <div className="line s_shimmer"></div>
                <div className="line s_shimmer"></div>
              </div>
            </div>
            <div className="box shimmer mb-3">
              <div className="lines">
                <div className="line s_shimmer"></div>
                <div className="line s_shimmer"></div>
                <div className="line s_shimmer"></div>
                <div className="line s_shimmer"></div>
              </div>
            </div>
            <div className="box shimmer">
              <div className="lines">
                <div className="line s_shimmer"></div>
                <div className="line s_shimmer"></div>
                <div className="line s_shimmer"></div>
                <div className="line s_shimmer"></div>
              </div>
            </div>
          </div>
          
          <Contacts />
          <Group />
          <Pages />
          
        </div>
      </div>
    </React.Fragment>
  );
};

export default RightChatWrapper;
