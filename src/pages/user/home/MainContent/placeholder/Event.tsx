import React from "react";
import {Link} from "react-router-dom";

const EventList: React.FC = () => {
  // Define an array of event objects
  const events = [
    {
      date: "FEB 22",
      title: "Meeting with clients",
      location: "41 madison ave, floor 24 new work, NY 10010",
    },
    {
      date: "APR 30",
      title: "Developer Programe",
      location: "41 madison ave, floor 24 new work, NY 10010",
    },
    {
      date: "APR 23",
      title: "Anniversary Event",
      location: "41 madison ave, floor 24 new work, NY 10010",
    },
  ];
  
  return (
    <React.Fragment>
      <div className="card w-100 shadow-xss rounded-xxl border-0 mb-3">
        <div className="card-body d-flex align-items-center p-4">
          <h4 className="fw-700 mb-0 font-xssss text-grey-900">Event</h4>
          <Link
            to={"/events"}
            className="fw-600 ms-auto font-xssss text-primary"
          >
            See all
          </Link>
        </div>
        {events.map((event, index) => (
          <div className="card-body d-flex pt-0 ps-4 pe-4 pb-3 overflow-hidden">
            <div className="bg-success me-2 p-3 rounded-xxl">
              <h4 className="fw-700 font-lg ls-3 lh-1 text-white mb-0">
                <span className="ls-1 d-block font-xsss text-white fw-600">
                  {event.date}
                </span>
                22
              </h4>
            </div>
            <h4 className="fw-700 text-grey-900 font-xssss mt-2">
              {event.title}
              <span className="d-block font-xsssss fw-500 mt-1 lh-4 text-grey-500">
                {event.location}
              </span>
            </h4>
          </div>
        ))}
      </div>
    </React.Fragment>
  );
};

export default EventList;
