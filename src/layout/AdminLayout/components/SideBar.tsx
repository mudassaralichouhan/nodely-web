import React, {useState} from 'react';
import {DesktopOutlined, FileOutlined, PieChartOutlined, TeamOutlined, UserOutlined,} from '@ant-design/icons';
import type {MenuProps} from 'antd';
import {Layout, Menu} from 'antd';

const {Sider} = Layout;

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem('Dashboard', '1', <PieChartOutlined/>),
  getItem('Posts', '2', <DesktopOutlined/>),
  getItem('User', 'sub-1', <UserOutlined/>, [
    getItem('Management', '3'),
    getItem('Password', '4'),
    getItem('Activity', '5'),
  ]),
  getItem('Group', 'sub-2', <TeamOutlined/>, [
    getItem('Public', '6'),
    getItem('Private', '7')
  ]),
  getItem('Page', 'page', <FileOutlined/>),
];

const SideBar: React.FC = () => {
  const [collapsed, setCollapsed] = useState(false);

  return (
    <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
      <div className="demo-logo-vertical"/>
      <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items}/>
    </Sider>
  );
};

export default SideBar;