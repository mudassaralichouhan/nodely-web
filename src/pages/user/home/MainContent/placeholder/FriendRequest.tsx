import React from "react";
import { Link } from "react-router-dom";

const list = [
  {
    title: 'Anthony Daugloi',
    mutual: 12,
    photo: '/assets/images/user-7.png',
  },
  {
    title: 'Mohannad Zitoun',
    mutual: 4,
    photo: '/assets/images/user-8.png',
  },
  {
    title: 'Ana Seary',
    mutual: 0,
    photo: '/assets/images/user-12.png',
  },
];

const FriendRequest: React.FC = () => {
  return (
    <React.Fragment>
      <div className="card w-100 shadow-xss rounded-xxl border-0 mb-3">
        <div className="card-body d-flex align-items-center p-4">
          <h4 className="fw-700 mb-0 font-xssss text-grey-900">
            Friend Request
          </h4>
          <Link
            to={"/requests"}
            className="fw-600 ms-auto font-xssss text-primary"
          >
            See all
          </Link>
        </div>
        {list.map((friend, index) => (
          <div key={index}>
            <div className="card-body d-flex pt-4 ps-4 pe-4 pb-0 border-top-xs bor-0">
              <figure className="avatar me-3">
                <img
                  src={friend.photo}
                  alt="image"
                  className="shadow-sm rounded-circle w45"
                />
              </figure>
              <h4 className="fw-700 text-grey-900 font-xssss mt-1">
                {friend.title}
                <span className="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">
                  {friend.mutual !== null && friend.mutual > 0
                    ? `${friend.mutual} mutual friend${friend.mutual !== 1 ? 's' : ''}`
                    : 'No mutual friends'}
                </span>
              </h4>
            </div>
            <div className="card-body d-flex align-items-center pt-0 ps-4 pe-4 pb-4">
              <Link
                to=""
                className="p-2 lh-20 w100 bg-primary-gradiant me-2 text-white text-center font-xssss fw-600 ls-1 rounded-xl"
              >
                Confirm
              </Link>
              <Link
                to=""
                className="p-2 lh-20 w100 bg-grey text-grey-800 text-center font-xssss fw-600 ls-1 rounded-xl"
              >
                Delete
              </Link>
            </div>
          </div>
        ))}
      </div>
    </React.Fragment>
  );
};

export default FriendRequest;
