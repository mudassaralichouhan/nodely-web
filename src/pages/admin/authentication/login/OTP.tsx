import React from "react";

import {Button, Form, Input, message, notification, Typography} from "antd";

import {PushpinOutlined} from "@ant-design/icons";
import {Link, useLocation, useNavigate} from "react-router-dom";
import {NotificationPlacement} from "antd/es/notification/interface";
import {AuthWrapper, useStyleProps} from "@/layout/AdminLayout/AuthLayout.tsx";
import {loginApi, LoginType} from "@/pages/admin/authentication/login/login-api.ts";

const {Text} = Typography;

const OTP: React.FC = () => {

  const [api, contextHolder] = notification.useNotification();
  const navigate = useNavigate();
  const location = useLocation();

  const styles = useStyleProps().styles;

  const onFinish = async (form: LoginType) => {

    const credentials = location?.state;

    const result = await loginApi({...credentials, ...form});

    if (!result.error) {
      message.success('Verification code sent on your mail!')
      if (result.data?.user)
        navigate('/admin/dashboard');

    } else {

      if (Array.isArray(result.data)) {
        result.data.forEach((entry) => {
          openNotification('bottomRight', entry.context.key, entry.message)
        });
      } else {
        openNotification('bottomRight', result.data.error, "")
      }
    }
  };

  const openNotification = (placement: NotificationPlacement, title: string, message: string) => {
    api.error({
      message: `Warning: ${title}`,
      description: message,
      placement,
    });
  };

  return (
    <AuthWrapper header={'OTP verification'}
                 description={'An OTP (One Time Password) is a temporary, secure PIN code that is sent to a user via SMS or email.'}>
      <Form
        initialValues={{
          remember: false,
        }}
        onFinish={onFinish}
        layout="vertical"
        requiredMark="optional"
      >
        <Form.Item
          name="otp"
          rules={[
            {
              required: true,
              message: "Please input your Name!",
            },
            {
              validator: (_: any, value: string) => {
                const isValid = /^\d{4}$/.test(value); // Check if the value is a 4-digit number
                if (!isValid) {
                  return Promise.reject('Please enter a valid 4-digit number.');
                }
                return Promise.resolve()
              }
            }
          ]}
        >
          <Input
            prefix={<PushpinOutlined/>}
            placeholder="Code"
            maxLength={4}
          />
        </Form.Item>
        <Form.Item style={{marginBottom: "0px"}}>
          <Button block="true" type="primary" htmlType="submit">
            Verify It
          </Button>
          <div style={styles.footer}>
            <Text style={styles.text}>Don't have an account?</Text>{" "}
            <Link to={"/admin/login"}>Sign in now</Link>
          </div>
        </Form.Item>
      </Form>
      {contextHolder}
    </AuthWrapper>
  );
};

export default OTP;
