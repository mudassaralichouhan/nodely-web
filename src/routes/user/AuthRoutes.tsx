import React, {lazy} from 'react';
import {Route, Routes as ReactRoutes} from 'react-router-dom';

import Loadable from "@/layout/SocialLayout/components/SocialLoadable.tsx";

const SocialLayout = Loadable(lazy(() => import('@/layout/SocialLayout')));
const AuthLogin = Loadable(lazy(() => import('@/pages/user/authentication/Login')));
const AuthRegister = Loadable(lazy(() => import('@/pages/user/authentication/Register')));

const publicRoutes = [
  {path: 'login', element: <AuthLogin/>},
  {path: 'register', element: <AuthRegister/>},
];

const AuthRoutes: React.FC = () => {
  return (
    <ReactRoutes>
      <Route path={'auth'} element={<SocialLayout/>}>
        {publicRoutes.map(route => (
          <Route key={route.path} path={route.path} element={route.element}/>
        ))}
      </Route>
    </ReactRoutes>
  );
}

export default AuthRoutes;
