import React from "react";
import Stories, {WithSeeMore} from "react-insta-stories";

export const StatusView = ({item}) => {
  
  const statusList = [
    {
      content: ({action, story}) => {
        return (
          <WithSeeMore story={story} action={action}>
            <div style={{background: "pink", padding: 20}}>
              <h1 style={{marginTop: "100%", marginBottom: 0}}>🌝</h1>
              <h1 style={{marginTop: 5}}>
                Here is the location journey story created for the shivam's
                journey.
              </h1>
            </div>
          </WithSeeMore>
        );
      },
      seeMoreCollapsed: ({toggleMore, action}) => (
        <p style={customSeeMore} onClick={() => toggleMore(true)}>
          A custom See More message →
        </p>
      ),
      seeMore: ({close}) => (
        <div
          style={{
            maxWidth: "100%",
            height: "100%",
            padding: 40,
            background: "white"
          }}
        >
          <h2>Just checking the see more feature.</h2>
          <p style={{textDecoration: "underline"}} onClick={close}>
            Go on, close this popup.
          </p>
        </div>
      ),
      duration: 700
    },
    // {
    //   content: ({action, isPaused}) => {
    //     return (
    //       <div style={contentStylestoryback}>
    //         <img style={image} src="https://i.ibb.co/MGbfDTH/Group-13.png"></img>
    //       </div>
    //     );
    //   }
    // },
  ];
  
  if (item.id === 1)
    statusList.push({
      content: ({action, story}) => {
        return (
          <WithSeeMore story={story} action={action}>
            <div style={{background: "pink", padding: 20}}>
              <h1 style={{marginTop: "100%", marginBottom: 0}}>🌝</h1>
              <h1 style={{marginTop: 5}}>
                Here is the location journey story created for the shivam's
                journey.
              </h1>
            </div>
          </WithSeeMore>
        );
      }
    });
  else if (item.id === 2)
    statusList.push({
      content: ({action, isPaused}) => {
        return (
          <div style={contentStyle}>
            <img
              style={image}
              src="https://picsum.photos/400/300"
            />
            <h4>stories creted by Cubestop travel app</h4>
          </div>
        );
      }
    });
  else if (item.id === 3)
    statusList.push({
      url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
      type: "video",
    });
  else if (item.id === 4)
    statusList.push({
      url: "https://picsum.photos/1080/1920",
      seeMore: ({close}) => (
        <div
          style={{
            maxWidth: "100%",
            height: "100%",
            padding: 40,
            background: "white"
          }}
        >
          <h2>Just checking the see more feature.</h2>
          <p style={{textDecoration: "underline"}} onClick={close}>
            Go on, close this popup.
          </p>
        </div>
      )
    });
  
  return (
    <div className="stories">
      <Stories
        keyboardNavigation
        defaultInterval={3000}
        stories={statusList}
        onStoryEnd={(s, st) => console.log("story ended", s, st)}
        onAllStoriesEnd={(s, st) => console.log("all stories ended", s, st)}
        onStoryStart={(s, st) => console.log("story started", s, st)}
      />
    </div>
  );
  
};

const image = {
  display: "block",
  maxWidth: "100%",
  borderRadius: 4
};

const contentStylestoryback = {
  background: "black",
  width: "100%",
  padding: 20,
  color: "white"
};

const contentStyle = {
  background: "salmon",
  width: "100%",
  padding: 20,
  color: "white"
};

const customSeeMore = {
  textAlign: "center",
  fontSize: 14,
  bottom: 20,
  position: "relative",
  color: "white"
};
export default StatusView;