import React, {lazy} from "react";
import {Route, Routes as ReactRoutes} from 'react-router-dom';

import Loadable from "@/layout/SocialLayout/components/SocialLoadable.tsx";

const SocialLayout = Loadable(lazy(() => import('@/layout/SocialLayout')));

const userRoutes = [
  {path: 'profile', element: <h1>User page</h1>},
];
const UserRoutes: React.FC = () => {
  return (
    <ReactRoutes>
      <Route path={''} element={<SocialLayout/>}>
        {userRoutes.map(route => (
          <Route key={route.path} path={route.path} element={route.element}/>
        ))}
      </Route>
    </ReactRoutes>
  );
}

export default UserRoutes;
