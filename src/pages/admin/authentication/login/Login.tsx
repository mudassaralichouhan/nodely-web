import React from "react";
import {Link, useNavigate} from "react-router-dom";

import {Button, Checkbox, Form, Input, message, notification, Typography} from "antd";

import {LockOutlined, MailOutlined} from "@ant-design/icons";
import {NotificationPlacement} from "antd/es/notification/interface";
import {loginApi, LoginType} from "@/pages/admin/authentication/login/login-api.ts";
import {AuthWrapper, useStyleProps} from "@/layout/AdminLayout/AuthLayout.tsx";

const {Text} = Typography;

const Login: React.FC = () => {

  const [api, contextHolder] = notification.useNotification();
  const navigate = useNavigate();

  const styles = useStyleProps().styles;

  const onFinish = async (form: LoginType) => {

    console.log(form);

    const result = await loginApi(form);

    if (!result.error) {
      message.success('Verification code sent on your mail!')
      navigate('/admin/otp', {
        state: form,
      });
    } else {

      if (Array.isArray(result.data)) {
        result.data.forEach((entry) => {
          openNotification('bottomRight', entry.context.key, entry.message)
        });
      } else {
        openNotification('bottomRight', result.data.error, "")
      }
    }
  };

  const openNotification = (placement: NotificationPlacement, title: string, message: string) => {
    api.error({
      message: `Warning: ${title}`,
      description: message,
      placement,
    });
  };

  return (
    <AuthWrapper header={'Sign in'}
                 description={'Welcome back to {import.meta.env.VITE_NAME}! Please enter your details below to sign in.'}>
      <Form
        name="normal_login"
        initialValues={{
          remember: false,
          email: 'kitihojodi@mailinator.com',
          password: 'Pa$$w0rd!'
        }}
        onFinish={onFinish}
        layout="vertical"
        requiredMark="optional"
      >
        <Form.Item
          name="email"
          rules={[
            {
              type: "email",
              required: true,
              message: "Please input your Email!",
            }
          ]}
        >
          <Input
            prefix={<MailOutlined/>}
            placeholder="Email"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your Password!",
            },
          ]}
        >
          <Input.Password
            prefix={<LockOutlined/>}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>
          <Link style={styles.forgotPassword} to={"/admin/password/forgot"}>
            Forgot password?
          </Link>
        </Form.Item>
        <Form.Item style={{marginBottom: "0px"}}>
          <Button block="true" type="primary" htmlType="submit">
            Log in
          </Button>
          <div style={styles.footer}>
            <Text style={styles.text}>Don't have an account?</Text>{" "}
            <Link to={"/admin/register"}>Sign up now</Link>
          </div>
        </Form.Item>
      </Form>
      {contextHolder}
    </AuthWrapper>
  );
};

export default Login;
